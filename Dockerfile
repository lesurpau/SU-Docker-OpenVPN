FROM debian:jessie
EXPOSE 1194/udp

WORKDIR ./

RUN apt-get update && apt-get -y install openvpn easy-rsa iptables

ADD ./ /home/

RUN cp /home/server.conf /etc/openvpn/server.conf && cp /home/sysctl.conf /etc/sysctl.conf && cp -r /usr/share/easy-rsa/ /etc/openvpn && mkdir /etc/openvpn/easy-rsa/keys && cp /home/vars /etc/openvpn/easy-rsa/vars && cp /home/keys/dh2048.pem /etc/openvpn/

RUN cp /home/keys/servername.crt /etc/openvpn/ && cp /home/keys/servername.key /etc/openvpn/ && cp /home/keys/ca.crt /etc/openvpn/

ENTRYPOINT bash /home/startScript.sh
